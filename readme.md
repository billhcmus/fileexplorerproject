## Thông tin cá nhân
### MSSV: 1512557
### Họ tên: Phan Trọng Thuyên
## Các chức năng đã làm được
1. Tạo ra TreeView bên trái, ListView bên phải.
2. Xét TreeView
> Tạo node root là This PC
Lấy danh sách các ổ đĩa trong máy bằng hàm GetLogicalDrives hoặc GetLogicalDriveStrings,
thêm các ổ đĩa vào node root, tạo sẵn thuộc tính cChildren = true để báo hiệu có các node con. 
Gán giá trị của ổ đĩa ví dụ C:\ vào PARAM (tag) để lấy lên xài lại.
Bắt sự kiện Expanding, lấy ra đường dẫn dấu ở PARAM để biết mình phải xư lí thư mục nào, 
duyệt nội dung thư mục bằng FindFirstFile & FindNextFile, chỉ lấy các thư mục để thêm vào làm node con.
3. Xét ListView
>Hiển thị toàn bộ thư mục và tập tin tương ứng với một đường dẫn Bấm đôi vào một thư mục sẽ thấy toàn bộ thư mục con và tập tin.
Tạo ra ListView có 4 cột: Tên, Loại, Thời gian chỉnh sửa, Dung lượng. 
Với ThisPC có 4 cột: Tên, Loại, Tổng dung lượng, Dung lượng trống.
Với thư mục có 3 cột Tên, Loại, thời gian chỉnh sửa. 
Với tập tin cần hiển thị 4 cột: Tên, Thời gian chỉnh sửa, Dung lượng, Loại.
Bấm đôi tệp tin thì sẽ khởi chạy.
Sử dụng icon hệ thống trích xuất từ shell32.dll, các icon ổ đĩa, this pc, folder, unknown file,...
## Các luồng sự kiện chính:
> Chạy chương trình, hiển thị treeview bên trái với root là ThisPC cùng 
các ổ đĩa là con của nó, bấm +/- để mở rộng hoặc thu gọn cây.
Ấn vào ổ đĩa khi ở trạng thái thu gọn sẽ xổ xuống các thư mục con tạo thành cây thư mục

>List view chứa chi tiết thư mục, ổ đĩa được chọn. Có các cột thông tin về tên, kích thước, loại, ngày chỉnh sửa gần nhất.
Double click vào thư mục trên listview sẽ mở thư mục đó và hiển thị các thư mục và tệp tin con của nó. Double click vào tệp tin sẽ mở tệp tin đó.
## Các luồng sự kiện phụ:
>Chương trình sẽ crash nếu double click vào vùng trống ở listview, do đó phải ràng buộc điều kiện khi double click.
## Các yêu cầu khác
- Nền tảng build: Visual Studio 2017
- Link bitbucket: https://billhcmus@bitbucket.org/billhcmus/windev.git
- Link Video: https://www.youtube.com/watch?v=DC5GLKuenz4&t=125s
- Link repo: https://billhcmus@bitbucket.org/billhcmus/fileexplorerproject.git