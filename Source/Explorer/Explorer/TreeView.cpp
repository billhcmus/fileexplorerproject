﻿#include "stdafx.h"
#include "TreeView.h"
#define MAXLEN 10240


HWND CreateATreeView(HWND hwndParent, long ID, HINSTANCE hInst, long lExtStyle, int x, int y, int nWidth, int nHeight, long lStyle)
{
	HWND hwndTV;
	HIMAGELIST *himgl = new HIMAGELIST;
	SetIconFromShell32(himgl);
	hwndTV = CreateWindowEx(lExtStyle, WC_TREEVIEW, TEXT("Tree View"), WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | WS_TABSTOP | lStyle, x, y, nWidth, nHeight, hwndParent, (HMENU)ID, hInst, NULL);
	TreeView_SetImageList(hwndTV, *himgl, TVSIL_NORMAL);
	return hwndTV;
}

LPCWSTR GetPath(HTREEITEM hItem, HWND hTreeView) {
	TVITEMEX tv; // giữ thuộc tính của treeview
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(hTreeView, &tv);
	return (LPCWSTR)tv.lParam;
}

void ExpandChild(HWND hTreeView, HTREEITEM hCur) {
	// sử dụng hCur đã lưu để lấy đường dẫn truy xuất vào thư mục
	HTREEITEM hCurSelChild = TreeView_GetChild(hTreeView, hCur); // Lấy con đầu tiên của treeview
	// có node con
	if (hCurSelChild != NULL) {
		do {
			if (TreeView_GetChild(hTreeView, hCurSelChild) == NULL) {
				LoadTreeViewItem(hCurSelChild, GetPath(hCurSelChild, hTreeView), hTreeView);
			}
		} while (hCurSelChild = TreeView_GetNextSibling(hTreeView, hCurSelChild));
	}
	else {
		LoadTreeViewItem(hCur, GetPath(hCur, hTreeView), hTreeView);
	}
}


void LoadTreeViewItem(HTREEITEM &hParent, LPCWSTR path, HWND hTreeView) {
	// lấy đường dẫn
	TCHAR buffer[MAXLEN];
	StrCpy(buffer, path);
	StrCat(buffer, L"\\*"); // đường dẫn tìm tất cả item

	// Chèn item mới vào Tree View
	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_SORT;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	// HIỆN TẠI BỎ QUA IMAGE

	WIN32_FIND_DATA fd; // chứa thông tin về file
	HANDLE hFindFile = FindFirstFileW(buffer, &fd); // tìm tất cả các file theo path

	if (hFindFile == INVALID_HANDLE_VALUE) {
		return;
	}
	TCHAR* folderPath;
	do {
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(fd.cFileName, L".") != 0 && StrCmp(fd.cFileName, L"..") != 0)) 
		{
			// file name
			tvInsert.item.pszText = fd.cFileName;
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			tvInsert.item.iSelectedImage = IDI_FOLDER;
			tvInsert.item.iImage = IDI_FOLDER;
			// set path for file
			StrCpy(folderPath, path);
			if (wcslen(path) != 3){
				StrCat(folderPath, L"\\");
			}
			StrCat(folderPath, fd.cFileName);

			tvInsert.item.lParam = (LPARAM)folderPath;

			HTREEITEM hItem = TreeView_InsertItem(hTreeView, &tvInsert);
		}		
	} while (FindNextFileW(hFindFile, &fd));
}

void loadThisPCToTree(Drive* drive, HWND hTreeView) {
	TV_INSERTSTRUCT tvInsert;

	// thêm mặt nạ, thay đổi text,icon, lparam
	tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_SELECTEDIMAGE;

	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.pszText = L"This PC";
	//tvInsert.item.iImage = ;
	tvInsert.item.iSelectedImage = 0;
	tvInsert.item.lParam = (LPARAM)L"This PC";
	HTREEITEM hThisPC = TreeView_InsertItem(hTreeView, &tvInsert);

	// Load volume
	for (int i = 0; i < drive->getCount(); i++) {
		tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
		tvInsert.hParent = hThisPC;
		tvInsert.item.pszText = drive->getDisplayName(i);
		tvInsert.item.lParam = (LPARAM)drive->getDriveLetter(i);
		TCHAR* checktype = new TCHAR[15];
		checktype = drive->GetType(i);
		
		if (StrCmp(checktype, DR_FIXED) == 0) {
			tvInsert.item.iImage = IDI_LOCALDISK;
			tvInsert.item.iSelectedImage = IDI_LOCALDISK;
		}
		else if (StrCmp(checktype, DR_REMOVABLE) == 0) {
			tvInsert.item.iSelectedImage = IDI_USB;
			tvInsert.item.iImage = IDI_USB;
		}
		else if (StrCmp(checktype, DR_REMOTE) == 0) {
			tvInsert.item.iSelectedImage = IDI_NETWORK_DRIVE;
			tvInsert.item.iImage = IDI_NETWORK_DRIVE;
		}
		else if (StrCmp(checktype, DR_CDROM) == 0) {
			tvInsert.item.iSelectedImage = IDI_CDROM;
			tvInsert.item.iImage = IDI_CDROM;
		}
		HTREEITEM hDrive = TreeView_InsertItem(hTreeView, &tvInsert);
		LoadTreeViewItem(hDrive, GetPath(hDrive, hTreeView), hTreeView);
	}
	TreeView_Expand(hTreeView, hThisPC, TVE_EXPAND);
	TreeView_SelectItem(hTreeView, hThisPC);
}
