﻿#pragma once
#include "resource.h"
#include <tchar.h>
#include <Windows.h>
#include <shellapi.h>
#include <CommCtrl.h>


#pragma comment(lib, "comctl32.lib")

#define DR_FIXED L"Local Disk"
#define DR_REMOVABLE L"Removable Drive"
#define DR_REMOTE L"Network Drive"
#define DR_CDROM L"CD-ROM"
#define IDI_THISPC 0
#define IDI_LOCALDISK 1
#define IDI_FOLDER 2
#define IDI_USB 3
#define IDI_NETWORK_DRIVE 4
#define IDI_CDROM 5
#define IDI_UNKNOW_FILE 6
// Struct chứa thông tin của các ổ đĩa
class Drive {
private:
	TCHAR** pszDriveLetter; // mảng chứa ký tự ổ đĩa A, B, C, D,....
	TCHAR** pszVolumeLabel; // Nhãn ổ đĩa
	TCHAR** pszDisplayName; // tên hiển thị ra ngoài ex: Data(D:)
	TCHAR** pszDriveType;
	int nCount; // số lượng ổ đĩa
public:
	int getCount();
	TCHAR* getDriveLetter(const int &);
	TCHAR* getVolumeLabel(const int &);
	TCHAR* getDisplayName(const int &);
	void GetSystemDrives();
	__int64 GetSize(const int &);
	LPWSTR TotalSizeToStr(const int &);
	__int64 GetFreeSize(const int &);
	LPWSTR FreeSizeToStr(const int &);
	TCHAR* GetType(const int &);
};

LPWSTR Convert(_int64);

void SetIconFromShell32(HIMAGELIST* himgList);

